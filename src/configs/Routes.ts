const Routes = {
  SplashSrc: 'SplashSrc',
  LoginSrc: 'LoginSrc',
  SignUp: 'SignUp',
  NoInternet: 'NoInternet',
  VerifyNumber: 'VerifyNumber',
  ForgotPass: 'ForgotPass',
  NewPassword: 'NewPassword',
};
export default Routes;
