 const Colors={
    primary:'#1A1D3A',
    secondary:'#F2F2F2',
    color_basic_1:'#000000',
    color_basic_2:'#181E3B',
    color_basic_3:'#FFFFFF',
    color_basic_4:'#E6E6E6',
    color_basic_5:'#0052E1',
    color_basic_6:'rgba(0, 0, 0, 0.54)'




}
export default Colors