const Icons = {
  back: require('assets/Icons/back.png'),
  ic_lock: require('assets/Icons/ic_lock.png'),
  ic_line: require('assets/Icons/ic_line.png'),
  ic_eye: require('assets/Icons/ic_eye.png'),
  ic_eyeoff: require('assets/Icons/ic_eyeoff.png'),
  ic_account: require('assets/Icons/ic_account.png'),
};
export default Icons;
