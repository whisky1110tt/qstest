import Colors from 'configs/Colors';
import React from 'react';
import {
  Text as DefaultText,
  TextStyle,
  TextProps,
  ColorValue,
  StyleSheet,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
interface Props extends TextProps {
  children?: string;
  color?: 'text' | 'text_placeholder' | 'text_active' | 'text_button';
  size?: TSize;
  type?: string;
  style?: TextStyle;
  onPress?(): null;
}
export type TSize = 'h1' | 'h2';
export enum Color {
  TEXT = 'text',
  TEXT_PLACEHOLDER = 'text_placeholder',
  TEXT_ACTIVE = 'text_active',
  TEXT_BUTTON = 'text_button',
}

const Size = {
  h1: {
    value: 22,
    lineHeight: 30,
  },
  h2: {
    value: 16,
    lineHeight: 22,
  },
};
const TextActive = ({color, size = 'h1', type, onPress, ...props}: Props) => {
  return (
    <TouchableOpacity onPress={onPress} activeOpacity={0.7}>
      <DefaultText
        style={[
          color === Color.TEXT
            ? styles.text
            : color === Color.TEXT_PLACEHOLDER
            ? styles.text_placeholder
            : color === Color.TEXT_ACTIVE
            ? styles.text_active
            : color === Color.TEXT_BUTTON
            ? styles.text_button
            : styles.text,
          {
            fontSize: Size[size].value,
            lineHeight: Size[size].lineHeight,
          },
          props.style,
        ]}>
        {props.children}
      </DefaultText>
    </TouchableOpacity>
  );
};
export default TextActive;
const styles = StyleSheet.create({
  text: {
    color: '#000000',
  },
  text_placeholder: {
    color: 'rgba(0, 0, 0, 0.54)',
  },
  text_active: {
    color: 'rgba(0, 82, 225, 1)',
  },
  text_button: {
    color: '#ffffff',
  },
});
