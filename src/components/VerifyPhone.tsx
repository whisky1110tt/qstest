import React, {useState} from 'react';
import {SafeAreaView, Text, StyleSheet, View, Dimensions} from 'react-native';

import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import Button from './Button';

const CELL_COUNT = 6;

const VerifyPhone = () => {
  const [value, setValue] = useState('');
  const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  return (
    <SafeAreaView style={styles.root}>
      <CodeField
        ref={ref}
        {...props}
        value={value}
        onChangeText={setValue}
        cellCount={CELL_COUNT}
        rootStyle={styles.codeFieldRoot}
        keyboardType="number-pad"
        textContentType="oneTimeCode"
        keyboardAppearance="light"
        renderCell={({index, symbol, isFocused}) => (
          <View>
            <Text
              key={index}
              style={styles.cell}
              onLayout={getCellOnLayoutHandler(index)}>
              {symbol || (isFocused ? <Cursor /> : null)}
            </Text>
            <View
              style={[styles.line, isFocused ? styles.line : styles.lineOut]}
            />
          </View>
        )}
      />
    </SafeAreaView>
  );
};

export default VerifyPhone;
const styles = StyleSheet.create({
  root: {
    marginTop: 46,
    marginLeft: 16,
  },
  title: {
    textAlign: 'center',
    fontSize: 30,
  },
  codeFieldRoot: {
    marginTop: 20,
    justifyContent: 'flex-start',
  },
  cell: {
    width: 40,
    height: 40,
    lineHeight: 29,
    fontSize: 24,
    textAlign: 'center',
  },
  line: {
    width: 32,
    height: 1,
    backgroundColor: '#D92728',
  },
  lineOut: {
    width: 32,
    height: 1,
    backgroundColor: '#181E3B',
  },
});
