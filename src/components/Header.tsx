import React, {memo} from 'react';
import {StyleSheet, View, TouchableOpacity, Dimensions} from 'react-native';
import Icon from './Icon';
import Text from './Text';
interface Props {
  title?: string;
  iconLeft?: string;
  iconRight?: string;
  onLeft?(): void;
  onRight?(): void;
}

const Header = memo(({title, iconLeft, iconRight, onLeft, onRight}: Props) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={onLeft}
        style={styles.button}>
        <Icon size={'small'} name={iconLeft} />
      </TouchableOpacity>
      <View style={styles.title}>
        <Text style={styles.text} size="h1" color="text" type={'bold'}>
          {title}
        </Text>
      </View>
      {!!iconRight && (
        <TouchableOpacity
          onPress={onRight}
          activeOpacity={0.7}
          style={styles.button}>
          <Icon style={styles.button} size={'large'} name={iconRight} />
        </TouchableOpacity>
      )}
      <View style={styles.line}></View>
    </View>
  );
});
export default Header;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginHorizontal: 16,
    paddingTop: 32 + 12,
    paddingBottom: 8,
  },
  text: {
    justifyContent: 'center',
    textAlign: 'center',
  },
  title: {
    flex: 1,
    height: 28,
    alignItems: 'center',
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  line: {
    backgroundColor: '#E6E6E6',
    width: Dimensions.get('window').width,
    height: 1,
    position: 'absolute',
    bottom: 0,
    right: -16,
  },
});
