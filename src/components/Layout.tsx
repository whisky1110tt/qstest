import React from 'react';
import {
  View as DefaultView,
  ViewProps,
  ViewStyle,
  PressableStateCallbackType,
} from 'react-native';
interface Props extends ViewProps {
  children?:
    | React.ReactNode
    | ((state: PressableStateCallbackType) => React.ReactNode);
  style?: ViewStyle;
}

const Layout = (props: Props) => {
  return (
    <DefaultView style={[{backgroundColor: '#FFFFFF'}, props.style]}>
      {props.children}
    </DefaultView>
  );
};
export default Layout;
