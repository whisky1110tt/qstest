import React from 'react';
import {Image, ImageStyle, StyleSheet} from 'react-native';
import Icons from 'assets/Icons';

interface IconProps {
  name?: string;
  size?: 'small' | 'medium' | 'large' | 'big' | 'huge';
  color?: string;
  style?: ImageStyle;
}

enum Size {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
  BIG = 'big',
  Huge = 'huge',
}

const Icon = ({name = 'back', size, color, style}: IconProps) => {
  return (
    <Image
      source={Icons[`${name}`]}
      style={[
        style,
        {
          tintColor: color,
        },
        size === Size.SMALL
          ? styles.small
          : size === Size.LARGE
          ? styles.large
          : size === Size.BIG
          ? styles.big
          : size === Size.Huge
          ? styles.huge
          : styles.medium,
      ]}
    />
  );
};
export default Icon;
const styles = StyleSheet.create({
  small: {
    width: 9,
    height: 14,
  },
  medium: {
    width: 14,
    height: 14,
  },
  large: {
    width: 22,
    height: 16,
  },
  big: {
    width: 16.76,
    height: 22,
  },
  huge: {
    width: 20,
    height: 20,
  },
});
