import React, {memo} from 'react';
import {StyleSheet, TouchableOpacity, View, ViewStyle} from 'react-native';
import Text from './Text';

interface props {
  title: string;
  width?: number;
  height?: number;
  bgColor?: BGColor;
  onPress?():void
  style?: ViewStyle;
}
export type BGColor = 'active' | 'nonActive';

const BGColor = {
  active: {
    backgroundColor: '#181E3B',
  },
  nonActive: {
    backgroundColor: '#E6E6E6',
  },
};

const Button = memo(
  ({title, width, height, bgColor = 'active', onPress, ...props}: props) => {
    return (
      <View>
        <TouchableOpacity
          onPress={onPress}
          style={[
            {
              width: width,
              height: height,
              backgroundColor: BGColor[bgColor].backgroundColor,
            },
            props.style,
          ]}>
          <Text color="text_placeholder">{title}</Text>
        </TouchableOpacity>
      </View>
    );
  },
);
export default Button;
