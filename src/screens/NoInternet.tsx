import {Images} from 'assets/image';
import Text from 'components/Text';
import React, {memo} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import Button from 'components/Button';
const NoInternet = memo(() => {
  return (
    <View style={styles.container}>
      <Image source={Images.Cloud} />
      <Text color="text" size="h1" style={styles.text}>
        Không có kết nối :(
      </Text>
      <Text color="text_placeholder" size="h2" style={styles.textPH}>
        Vui lòng thử lại sau
      </Text>
      <Button
        title="THỬ LẠI"
        width={200}
        height={48}
        bgColor="active"
        style={styles.button}
      />
    </View>
  );
});
export default NoInternet;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  text: {
    marginTop: 24,
  },
  textPH: {
    marginTop: 6,
  },
});
