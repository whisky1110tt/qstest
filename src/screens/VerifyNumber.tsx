import {useNavigation} from '@react-navigation/native';
import Button from 'components/Button';
import Header from 'components/Header';
import Layout from 'components/Layout';
import Text from 'components/Text';
import TextActive from 'components/TextActive';
import VerifyPhone from 'components/VerifyPhone';
import React, {memo} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
const VerifyNumber = memo(() => {
  const navigation = useNavigation();
  return (
    <Layout style={styles.container}>
      <Header title="Nhập mã xác thực" onLeft={() => navigation.goBack()} />
      <Text size="h2" style={styles.text}>
        Mã xác thực được gửi đến số
      </Text>
      <Text size="h1" style={styles.text}>
        +84 358287646
      </Text>
      <VerifyPhone />
      <Text size="h2" style={styles.note}>
        Mã xác thực không chính xác
      </Text>
      <TextActive style={styles.textActive} color="text_active">
        Gửi lại mã xác thực
      </TextActive>
      <Button title="Tiếp" style={styles.button} />
    </Layout>
  );
});
export default VerifyNumber;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textInput: {
    letterSpacing: 16,
    fontSize: 24,
  },
  line: {
    backgroundColor: 'red',
    height: 1,
    width: '75%',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: Dimensions.get('window').width,
    height: 40,
    marginBottom: 36,
    position: 'absolute',
    top: 120,
  },
  text: {
    marginTop: 22.5,
    marginHorizontal: 16,
  },
  note: {
    color: 'rgba(217, 39, 40, 1)',
    marginLeft: 16,
    marginTop: 12,
  },
  textActive: {
    marginLeft: 16,
    marginTop: 26,
  },
});
