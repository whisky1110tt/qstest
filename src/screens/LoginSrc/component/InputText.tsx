import React, {memo} from 'react';
import {Dimensions, StyleSheet, View} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import scaleAccordingToDevice from 'utils/scaleAccordingToDevice';
import Icon from '../../../components/Icon';
import Layout from '../../../components/Layout';
interface InputProps {
  title: string;
  icon: string;
}
const InputText = memo(({title, icon}: InputProps) => {
  return (
    <Layout style={styles.container}>
      <Icon
        name={icon}
        color="rgba(0, 0, 0, 0.54)"
        size="huge"
        style={styles.icon}
      />
      <TextInput placeholder={title} style={styles.textInput} />
      <View style={styles.line}></View>
    </Layout>
  );
});
export default InputText;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginHorizontal: 16,
    marginTop: 32,
    height: 32,
  },
  textInput: {
    marginLeft: 16,
    width: scaleAccordingToDevice(300),
  },
  line: {
    backgroundColor: '#E6E6E6',
    width: Dimensions.get('window').width - 32,
    height: 1,
    position: 'absolute',
    bottom: -12,
  },
  icon: {
    marginTop: 6,
  },
});
