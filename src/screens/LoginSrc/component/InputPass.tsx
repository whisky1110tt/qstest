import {useNavigation} from '@react-navigation/native';
import Routes from 'configs/Routes';
import React, {memo, useState} from 'react';
import {Dimensions, StyleSheet, View} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import Icon from '../../../components/Icon';
import Layout from '../../../components/Layout';
import Text from '../../../components/Text';
interface InputProps {
  title: string;
  iconLeft: string;
  iconRight: string;
}
const InputPass = memo(({title, iconLeft, iconRight}: InputProps) => {
  const [invisible, setInvisible] = useState(true);
  const navigation = useNavigation();
  return (
    <Layout style={styles.container}>
      <View style={{flexDirection: 'row'}}>
        <Icon
          name={iconLeft}
          color="rgba(0, 0, 0, 0.54)"
          size="big"
          style={styles.icon}
        />
        <TextInput
          placeholder={title}
          style={styles.textInput}
          secureTextEntry={invisible}
        />
      </View>

      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <TouchableOpacity onPress={() => setInvisible(!invisible)}>
          <Icon
            name={invisible ? 'ic_eyeoff' : 'ic_eye'}
            color="rgba(0, 0, 0, 0.54)"
            size="large"
          />
        </TouchableOpacity>
        <View style={styles.line2} />
        <TouchableOpacity
          onPress={() => navigation.navigate(Routes.ForgotPass)}>
          <Text size="h2" color="text_active">
            Quên?
          </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.line} />
    </Layout>
  );
});
export default InputPass;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginHorizontal: 16,
    marginTop: 16,
    height: 32,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textInput: {
    marginLeft: 16,
    width: '60%',
  },
  line: {
    backgroundColor: '#E6E6E6',
    width: Dimensions.get('window').width - 32,
    height: 1,
    position: 'absolute',
    bottom: -12,
  },
  line2: {
    backgroundColor: '#E6E6E6',
    width: 1,
    height: 16,
    marginHorizontal: 16,
  },
  icon: {
    marginTop: 4,
    marginLeft: 4,
  },
});
