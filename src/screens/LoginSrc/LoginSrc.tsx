import {useNavigation} from '@react-navigation/native';
import {Images} from 'assets/image';
import Header from 'components/Header';
import InputPass from 'screens/LoginSrc/component/InputPass';
import InputText from 'screens/LoginSrc/component/InputText';
import Layout from 'components/Layout';
import NonActiveButton from 'components/NonActiveButton';
import Text from 'components/Text';
import Routes from 'configs/Routes';
import React, {memo} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import scaleAccordingToDevice from 'utils/scaleAccordingToDevice';
const LoginSrc = memo(() => {
  const [text, onChangeText] = React.useState('Email/Số điện thoại');
  const [phoneLogin, setPhoneLogin] = React.useState(true);
  const navigation = useNavigation();
  return (
    <Layout style={styles.container}>
      <Header
        iconLeft="back"
        title="Đăng nhập"
        onLeft={() => navigation.goBack()}
      />
      <Image source={Images.Logo} style={styles.logo} />
      <InputText
        icon="ic_account"
        title={phoneLogin ? 'Email/Số điện thoại' : 'Số điện thoại'}
      />
      {phoneLogin ? (
        <InputPass iconLeft="ic_lock" title="Mật khẩu" iconRight="ic_eye" />
      ) : null}
      <NonActiveButton
        title="Đăng nhập"
        bgColor="nonActive"
        style={styles.button}
        onPress={() => navigation.navigate(Routes.VerifyNumber)}
      />
      <View style={styles.signIn}>
        <TouchableOpacity onPress={() => navigation.navigate(Routes.SignUp)}>
          <Text color="text_active" size="h2">
            Đăng ký
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setPhoneLogin(!phoneLogin)}>
          <Text color="text_active" size="h2">
            Đăng nhập bằng SMS
          </Text>
        </TouchableOpacity>
      </View>
    </Layout>
  );
});
export default LoginSrc;
const width = scaleAccordingToDevice(343);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  logo: {
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 31.5,
  },
  textInput: {
    width: scaleAccordingToDevice(343),
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    width: scaleAccordingToDevice(343),
    height: 40,
    marginTop: 24,
  },
  signIn: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: width,
    marginTop: 16,
  },
});
