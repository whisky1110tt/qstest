import {useNavigation} from '@react-navigation/native';
import Routes from 'configs/Routes';
import React, {memo} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {NavigationStackProp} from 'react-navigation-stack';
import {Images} from '../assets/image';
const SplashSrc = memo(() => {
  const navigation = useNavigation<NavigationStackProp>();

  return (
    <View style={styles.container}>
      <Image source={Images.CoedoSplash} style={styles.image} />
      <TouchableOpacity onPress={() => navigation.navigate('LoginSrc')}>
        <Text>Go</Text>
      </TouchableOpacity>
    </View>
  );
});
export default SplashSrc;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1A1D3A',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 102,
    height: 112,
  },
});
