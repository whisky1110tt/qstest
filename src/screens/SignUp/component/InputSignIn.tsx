import React, {memo} from 'react';
import {Dimensions, StyleSheet, View} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import scaleAccordingToDevice from 'utils/scaleAccordingToDevice';
import Layout from 'components/Layout';
import Text from 'components/Text';
interface InputProps {
  title: string;
  placeholder: string;
}
const InputSignIn = memo(({title, placeholder}: InputProps) => {
  return (
    <Layout style={styles.container}>
      <Text size="h2">{title}</Text>
      <TextInput placeholder={placeholder} style={styles.textInput} />
      <View style={styles.line}></View>
    </Layout>
  );
});
export default InputSignIn;
const styles = StyleSheet.create({
  container: {
    marginLeft: 16,
    marginTop: 32,
    height: 32,
  },
  textInput: {
    width: scaleAccordingToDevice(300),
  },
  line: {
    backgroundColor: '#E6E6E6',
    width: Dimensions.get('window').width,
    height: 1,
    position: 'absolute',
    bottom: -12,
  },
});
