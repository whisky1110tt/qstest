import {useNavigation} from '@react-navigation/native';
import Header from 'components/Header';
import Layout from 'components/Layout';
import Text from 'components/Text';
import React, {memo} from 'react';
import {StyleSheet} from 'react-native';
import InputSignIn from './component/InputSignIn';
const SignUp = memo(() => {
  const navigation = useNavigation();
  return (
    <Layout style={styles.container}>
      <Header title="Đăng ký" onLeft={() => navigation.goBack()} />
      <InputSignIn title="Họ và tên" placeholder="Nhập họ và tên" />
      <InputSignIn title="Số điện thoại" placeholder="Nhập số điện thoại" />
      <InputSignIn title="Email" placeholder="email@address.com" />
      <InputSignIn title="Mật khẩu" placeholder="Nhập mật khẩu" />
    </Layout>
  );
});
export default SignUp;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
