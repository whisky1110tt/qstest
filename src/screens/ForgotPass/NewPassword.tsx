import {useNavigation} from '@react-navigation/native';
import Header from 'components/Header';
import Layout from 'components/Layout';
import React, {memo} from 'react';
import {StyleSheet} from 'react-native';
import InputSignIn from 'screens/SignUp/component/InputSignIn';
const NewPassword = memo(() => {
  const navigation = useNavigation();
  return (
    <Layout style={styles.container}>
      <Header title="Đặt lại mật khẩu" onLeft={() => navigation.goBack()} />
      <InputSignIn title="Mật khẩu mới" placeholder="Nhập mật khẩu" />
    </Layout>
  );
});
export default NewPassword;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
