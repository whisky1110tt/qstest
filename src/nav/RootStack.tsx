import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import React, {memo} from 'react';
import {KeyboardAvoidingView, Platform, StyleSheet, View} from 'react-native';
import Routes from 'configs/Routes';
import SplashSrc from 'screens/SplashSrc';
import NoInternet from 'screens/NoInternet';
import LoginSrc from 'screens/LoginSrc/LoginSrc';
import SignUp from 'screens/SignUp/SignUp';
import VerifyNumber from 'screens/VerifyNumber';
import ForgotPass from 'screens/ForgotPass/ForgotPass';
import NewPassword from 'screens/ForgotPass/NewPassword';
const Stack = createStackNavigator();
const RootStack = memo(() => {
  return (
    <KeyboardAvoidingView
      style={styles.container}
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <Stack.Navigator
        initialRouteName={Routes.LoginSrc}
        screenOptions={{
          headerShown: false,
          ...TransitionPresets.SlideFromRightIOS,
        }}>
        <Stack.Screen name={Routes.SplashSrc} component={SplashSrc} />
        <Stack.Screen name={Routes.NoInternet} component={NoInternet} />
        <Stack.Screen name={Routes.LoginSrc} component={LoginSrc} />
        <Stack.Screen name={Routes.SignUp} component={SignUp} />
        <Stack.Screen name={Routes.VerifyNumber} component={VerifyNumber} />
        <Stack.Screen name={Routes.ForgotPass} component={ForgotPass} />
        <Stack.Screen name={Routes.NewPassword} component={NewPassword} />
      </Stack.Navigator>
    </KeyboardAvoidingView>
  );
});
export default RootStack;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
