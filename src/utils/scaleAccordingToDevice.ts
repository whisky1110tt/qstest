import { Dimensions } from "react-native";

const width=Dimensions.get('window').width;
const height=Dimensions.get('window').height;


const scaleAccordingToDevice = (
    value: number,
    accordingHeight: boolean = false,
  ) => {
    let ratio = width / 375;
    if (accordingHeight) {
      ratio = height / 812;
    }
    const convertValue = value * ratio;
  
    return convertValue;
  };
  export default scaleAccordingToDevice;
