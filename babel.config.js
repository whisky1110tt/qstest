module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: [
          '.ios.ts',
          '.android.ts',
          '.ts',
          '.ios.tsx',
          '.android.tsx',
          '.tsx',
          '.jsx',
          '.js',
          '.json',
        ],
        alias: {
          '@config': './src/config/index',
          '@config/*': './src/config/*',
          '@nav': './src/nav',
          '@screen': './src/screens/*',
          '@theme': './src/theme',
          '@theme/*': './src/theme/*',
          '@utils': './src/utils',
          '@utils/*': './src/utils/*',
          '@image/*': './src/asset/image/*',
          '@components': './src/components',
          '@components/*': 'src/components/*',
          '@assets/*': 'src/assets/*',
          '@i18n': './src/utils/i18n/index',
        },
      },
    ],
  ],
};
